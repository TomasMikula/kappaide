name := "kappaide"

version := "0.2.5"

scalaVersion := "2.11.7"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

javacOptions ++= Seq(
  "-source", "1.8",
  "-target", "1.8",
  "-Xlint:unchecked",
  "-Xlint:deprecation")

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies ++= Seq(
  "org.kappalanguage" %% "kapparser" % "0.1.2",
  "org.reactfx" % "reactfx" % "2.0-M4u1",
  "org.fxmisc.richtext" % "richtextfx" % "0.6.9",
  "org.fxmisc.easybind" % "easybind" % "[1.0.3,)",
  "org.fxmisc.livedirs" % "livedirsfx" % "1.0.0-SNAPSHOT",
  "org.fxmisc.backcheck" % "backcheckfx" % "1.0.0-SNAPSHOT"
)

// include src/main/resources as source folder
EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource

fork := true

mainClass in Compile := Some("kappa.ide.ui.App")

jfxSettings

JFX.mainClass := Some("kappa.ide.ui.App")

JFX.javafx := Some("8.0")

JFX.j2se := Some("8.0")

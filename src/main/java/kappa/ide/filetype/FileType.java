package kappa.ide.filetype;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.image.Image;

public class FileType {

    private static final Map<String, FileType> knownTypes = new HashMap<>();

    public static final FileType KAPPA = define("kappa-file-16.png", "ka", "kappa");
    public static final FileType TEXT = define("text-file-16.png", "txt");
    public static final FileType UNRECOGNIZED = define("text-file-16.png");

    /**
     * Returns the extension of the given file name, or an empty string
     * if the given file name does not have an extension.
     */
    public static String ext(Path path) {
        String name = path.getFileName().toString();
        int i = name.lastIndexOf(".");
        return i == -1 ? "" : name.substring(i + 1);
    }

    /**
     * Returns the type of the given file, or `FileType.UNRECOGNIZED`
     * if not recognized as one of known file types.
     */
    public static FileType of(Path f) {
        synchronized(knownTypes) {
            return knownTypes.getOrDefault(ext(f), UNRECOGNIZED);
        }
    }

    /**
     * Defines a new file type and registers it as a known file type
     * with each of the given extensions.
     * @return the newly defined file type
     */
    public static FileType define(Image icon, String... extensions) {
        FileType ft = new FileType(Arrays.asList(extensions), icon);
        synchronized(knownTypes) {
            for(String ext: extensions) {
                knownTypes.put(ext, ft);
            }
        }
        return ft;
    }

    private static FileType define(String icon, String... extensions) {
        Image image = new Image(FileType.class.getResource(icon).toString());
        return define(image, extensions);
    }


    private final List<String> extensions;
    private final Image icon;

    private FileType(List<String> ext, Image icon) {
        this.extensions = ext;
        this.icon = icon;
    }

    public boolean isTypeOf(Path f) {
        return extensions.contains(ext(f));
    }

    public Image getIcon() {
        return icon;
    }
}
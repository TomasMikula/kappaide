package kappa.ide.parsing;

import static kappa.ide.util.Lazy.*;

import java.util.Optional;

import kappa.ide.filetype.FileType;
import kappa.ide.util.Lazy;

public class StyleSheets {
    private static final Lazy<String> KAPPA = lazily(() -> StyleSheets.class.getResource("kappa-syntax.css").toExternalForm());

    public static Optional<String> get(FileType ft) {
        if(ft.equals(FileType.KAPPA)) {
            return Optional.of(KAPPA.get());
        } else {
            return Optional.empty();
        }
    }
}

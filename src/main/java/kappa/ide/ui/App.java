package kappa.ide.ui;

import static kappa.ide.util.Lazy.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import kappa.ide.filetype.FileType;
import kappa.ide.parsing.AstUtil;
import kappa.ide.parsing.Highlighting;
import kappa.ide.parsing.KappaParser;
import kappa.ide.ui.editor.EditorPane;
import kappa.ide.ui.editor.EditorTab;
import kappa.ide.ui.filetree.ProjectBrowser;
import kappa.ide.util.Lazy;
import kappa.ide.util.ScalableContentPane;
import kappa.ide.visualization.RuleVisualization;
import kappa.syntax.KappaFile;
import kappa.syntax.Rule;

import org.fxmisc.backcheck.AnalysisManager;
import org.fxmisc.backcheck.AnalysisManager.Update;
import org.fxmisc.backcheck.Analyzer;
import org.fxmisc.backcheck.AsyncAnalyzer;
import org.fxmisc.backcheck.InputChange;
import org.fxmisc.backcheck.InputChange.InputAddition;
import org.fxmisc.backcheck.InputChange.InputModification;
import org.fxmisc.backcheck.InputChange.InputRemoval;
import org.fxmisc.easybind.EasyBind;
import org.fxmisc.easybind.monadic.MonadicBinding;
import org.fxmisc.livedirs.DirectoryModel;
import org.fxmisc.livedirs.LiveDirs;
import org.reactfx.AwaitingEventStream;
import org.reactfx.EventSource;
import org.reactfx.EventStream;
import org.reactfx.EventStreams;
import org.reactfx.SuspendableNo;
import org.reactfx.util.Try;

import scala.util.parsing.combinator.Parsers.ParseResult;


public class App extends Application {

    private static enum ChangeInitiator {
        EXTERNAL,
        BROWSER,
        EDITOR,
    }

    private static final String[] STYLESHEETS = new String[] {
        EditorPane.class.getResource("editor-pane.css").toExternalForm(),
        RuleVisualization.class.getResource("visualization.css").toExternalForm()
    };

    private static final Lazy<ScheduledExecutorService> EXECUTOR_SERVICE = lazily(
            () -> Executors.newSingleThreadScheduledExecutor(), // provider
            es -> es.shutdown()); // disposer

    public static void main(String[] args) {
        launch(args);
    }

    public static ScheduledExecutorService getExecutorService() {
        return EXECUTOR_SERVICE.get();
    }

    private final Function<ParseResult<KappaFile>, Highlighting> computeHighlighting =
            Highlighting::of;

    private LiveDirs<ChangeInitiator> liveDirs;
    private EditorPane editorPane;
    private AnalysisManager<Path, String, ParseResult<KappaFile>> analysisManager;

    @Override
    public void start(Stage primaryStage) throws IOException {
        // display splash screen
        SplashScreen splash = new SplashScreen();
        splash.show();

        // check command line arguments
        if(getParameters().getUnnamed().size() > 1) {
            System.err.println("Too many command-line arguments");
            Platform.exit();
        }

        // obtain project directory,
        // either from command line or fire up project chooser
        Optional<Path> projectDir;
        switch(getParameters().getUnnamed().size()) {
            case 0:
                projectDir = ProjectChooser.showDialog(splash);
                break;
            case 1:
                Path dir = Paths.get(getParameters().getUnnamed().get(0));
                if(Files.isDirectory(dir)) {
                    projectDir = Optional.of(dir);
                } else {
                    System.err.println(dir + " is not a directory");
                    projectDir = Optional.empty();
                }
                break;
            default:
                throw new AssertionError("unreachable code");
        }

        // switch to main window or exit
        if(projectDir.isPresent()) {
            initMainStage(primaryStage, projectDir.get());
            splash.hide();
            primaryStage.show();
        } else {
            Platform.exit();
        }
    }

    @Override
    public void stop() {
        EXECUTOR_SERVICE.close();
        if(liveDirs != null) {
            liveDirs.dispose();
        }
    }

    private void initMainStage(Stage stage, Path projectDir) throws IOException {
        analysisManager = createAnalysisManager();
        liveDirs = createLiveDirs();
        editorPane = new EditorPane(
                liveDirs.io().withInitiator(ChangeInitiator.EDITOR));
        ProjectBrowser fileBrowser = new ProjectBrowser(
                liveDirs.model().getRoot(),
                liveDirs.io().withInitiator(ChangeInitiator.BROWSER));
        ScalableContentPane rulePane = new ScalableContentPane();
        AwaitingEventStream<List<InputChange<Path, String>>> kappaFileChanges =
                initKappaFileChanges();
        EventStream<Try<Update<Path, Highlighting>>> highlightings =
                analysisManager.transformedResults((path, ast) -> computeHighlighting.apply(ast));
        SuspendableNo tabSwitching = new SuspendableNo();
        EventStream<EditorTab> activeTab =
                EventStreams.valuesOf(editorPane.activeTabProperty())
                        .filter(tab -> tab != null)
                        .suspenderOf(tabSwitching);
        EventStream<Integer> activeFilePosition = activeTab.flatMap(tab ->
                EventStreams.valuesOf(tab.caretPositionProperty()));
        EventStream<ParseResult<KappaFile>> activeFileAst = activeTab.<Try<ParseResult<KappaFile>>>flatMap(
                tab -> analysisManager.transformedResults(tab.getPath(), ast -> ast))
                .hook(t -> { if(t.isFailure()) t.getFailure().printStackTrace(); })
                .filterMap(Try::toOptional);
        MonadicBinding<Boolean> activeEditorBusy =
                editorPane.activeTabProperty()
                        .flatMap(tab -> tab.beingUpdatedProperty())
                        .orElse(false);
        MonadicBinding<Boolean> processing = EasyBind.combine(
                kappaFileChanges.pendingProperty(),
                analysisManager.busyProperty(),
                tabSwitching,
                activeEditorBusy,
                (a, b, c, d) -> a || b || c || d);
        EventStream<Boolean> doneProcessing = EventStreams.valuesOf(processing).filter(x -> !x);
        EventStream<Rule> activeRule = EventStreams.combine(activeFileAst, activeFilePosition)
                .emitOn(doneProcessing)
                .<Optional<Rule>>map(t -> t.map((ast, pos) -> AstUtil.ruleAt(ast, pos)))
                .filterMap(Function.identity());

        kappaFileChanges.subscribe(changes -> {
            analysisManager.handleFileChanges(changes);
        });
        highlightings.subscribe(h -> {
            h.ifSuccess(update -> {
                applyHighlighting(update.getFileId(), update.getResult());
            });
            h.ifFailure(Throwable::printStackTrace);
        });
        activeRule.map(RuleVisualization::apply)
                .subscribe(rulePane::setContent);
        fileBrowser.doubleClickedFiles()
                .subscribe(this::openAndHighlight);

        liveDirs.model().deletions().subscribe(
                update -> editorPane.forceClose(update.getPath()));
        liveDirs.model().creations().subscribe(creation -> {
            if(creation.getInitiator() == ChangeInitiator.BROWSER) {
                Path path = creation.getPath();
                if(Files.isRegularFile(path)) {
                    openAndHighlight(path);
                }
            }
        });

        liveDirs.addTopLevelDirectory(projectDir);

        stage.setOnCloseRequest(evt -> handleCloseRequest(stage, evt));

        StackPane fileBrowserContainer = new StackPane(fileBrowser.getNode());
        StackPane editorPaneContainer = new StackPane(editorPane.getNode());
        StackPane rulePaneContainer = new StackPane(rulePane);

        SplitPane right = new SplitPane();
        right.setOrientation(Orientation.VERTICAL);
        right.getItems().addAll(editorPaneContainer, rulePaneContainer);

        SplitPane center = new SplitPane();
        center.getItems().addAll(fileBrowserContainer, right);

        BorderPane root = new BorderPane(center);

        Scene scene = new Scene(root, 600, 450);
        scene.getStylesheets().addAll(STYLESHEETS);

        stage.setTitle("Kappa IDE");
        stage.setScene(scene);
    }

    private AwaitingEventStream<List<InputChange<Path, String>>> initKappaFileChanges() {

        // new Kappa files
        EventSource<InputAddition<Path, String>> newKappaFiles = new EventSource<>();
        liveDirs.model().creations().subscribe(creation -> {
            Path path = creation.getPath();
            if(isKappaFile(path)) {
                liveDirs.io().loadUTF8File(path).whenCompleteAsync((content, error) -> {
                    if(content != null) {
                        newKappaFiles.push(new InputAddition<>(path, content));
                    } else {
                        // TODO: log
                    }
                },
                Platform::runLater);
            }
        });

        // removed Kappa files
        EventStream<InputRemoval<Path, String>> goneKappaFiles =
                liveDirs.model().deletions().filterMap(
                        this::isKappaFile,
                        deletion -> new InputRemoval<>(deletion.getPath()));

        // Kappa file changes
        EventStream<InputModification<Path, String>> kappaFileChanges =
                editorPane.changeStream()
                        .filter(ch -> isKappaFile(ch.getInputId()));

        // when file is closed without saving changes, reset to the on-disk state
        EventSource<InputModification<Path, String>> closedUnsavedKappaFiles = new EventSource<>();
        editorPane.closingTabs().subscribe(t -> {
            if(t.isChanged()) {
                Path file = t.getPath();
                if(liveDirs.model().contains(file)) { // not closed due to disappearance
                    liveDirs.io().loadUTF8File(file).thenAcceptAsync(
                            content -> closedUnsavedKappaFiles.push(new InputModification<>(file, content)),
                            Platform::runLater);
                }
            }
        });

        // TODO: should also handle external modifications (not originating from editorPane)

        // merged and delayed
        return EventStreams.merge(newKappaFiles, goneKappaFiles, kappaFileChanges, closedUnsavedKappaFiles)
                .reduceSuccessions(
                        () -> new ArrayList<InputChange<Path, String>>(),
                        (list, change) -> { list.add(change); return list; },
                        Duration.ofMillis(300));
    }

    private void handleCloseRequest(Stage stage, WindowEvent evt) {
        if(editorPane.hasUnsavedChangesInAnyTab()) {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.initOwner(stage);
            alert.setTitle("Save changes before closing?");
            alert.setHeaderText("Some files have unsaved changes.");
            alert.setContentText("Save before closing?");
            alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);

            ButtonType res = alert.showAndWait().get();
            if(res == ButtonType.YES) {
                evt.consume();
                editorPane.saveAll().whenCompleteAsync((none, error) -> {
                    if(error == null) {
                        stage.close();
                    } else {
                        // TODO: log
                    }
                },
                Platform::runLater);
            } else if(res == ButtonType.CANCEL) {
                evt.consume();
            } else {
                // don't consume the event, proceed without saving
            }
        }
    }

    private boolean isKappaFile(Path path) {
        return Files.isRegularFile(path) && FileType.KAPPA.isTypeOf(path);
    }

    private boolean isKappaFile(DirectoryModel.Update<ChangeInitiator> update) {
        return isKappaFile(update.getPath());
    }

    private void applyHighlighting(Path file, Highlighting highlighting) {
        editorPane.applyHighlighting(file, highlighting);
    }

    private void openAndHighlight(Path file) {
        editorPane.open(file).whenCompleteAsync((none, error) -> {
            if(error == null) {
                highlight(file);
            } else {
                error.printStackTrace();
            }
        }, Platform::runLater);
    }

    private void highlight(Path file) {
        analysisManager.withTransformedResult(
                file, computeHighlighting, highlighting -> this.applyHighlighting(file, highlighting))
        .whenComplete((found, ex) -> {
            if(ex != null) {
                ex.printStackTrace();
            } else if(!found) {
                // file not yet analyzed, do nothing
            }
        });
    }

    private static AnalysisManager<Path, String, ParseResult<KappaFile>> createAnalysisManager() {
        Analyzer<Path, String, ParseResult<KappaFile>> analyzer =
                Analyzer.<Path, String, ParseResult<KappaFile>>wrap(new KappaParser());
        AsyncAnalyzer<Path, String, ParseResult<KappaFile>> asyncAnalyzer =
                AsyncAnalyzer.from(analyzer, getExecutorService());
        return AnalysisManager.manageFromFxThread(asyncAnalyzer);
    }

    private static LiveDirs<ChangeInitiator> createLiveDirs() throws IOException {
        LiveDirs<ChangeInitiator> liveDirs = new LiveDirs<>(ChangeInitiator.EXTERNAL);
        liveDirs.model().setGraphicFactory(ProjectBrowser.ICON_FACTORY);
        return liveDirs;
    }
}

package kappa.ide.ui;

import java.io.File;
import java.nio.file.Path;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;

class ProjectChooser {

    public static Optional<Path> showDialog(Window owner) {
        return new ProjectChooser().showDlg(owner);
    }

    // selected project directory
    private Optional<Path> projectDir = Optional.empty();

    private ProjectChooser() {
        // private constructor to prevent instantiation
    }

    private Optional<Path> showDlg(Window owner) {
        ButtonType browse = new ButtonType("Browse", ButtonData.OK_DONE);

        Alert alert = new Alert(AlertType.INFORMATION);
        alert.initOwner(owner);
        alert.setHeaderText("Select project directory");
        alert.setTitle("Kappa IDE: Project selection");
        alert.getButtonTypes().setAll(browse, ButtonType.CANCEL);

        ((Button) alert.getDialogPane().lookupButton(browse))
                .addEventFilter(ActionEvent.ACTION, ae -> {
                    File f = new DirectoryChooser().showDialog(alert.getOwner());
                    if(f != null) {
                        projectDir = Optional.of(f.toPath());
                    } else {
                        ae.consume();
                    }
                });

        alert.showAndWait();

        return projectDir;
    }
}

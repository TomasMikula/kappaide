package kappa.ide.ui;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class SplashScreen extends Stage {

    public SplashScreen() {
        super(javafx.stage.StageStyle.UNDECORATED);
        String imageUrl = SplashScreen.class.getResource("splash.png").toString();
        ImageView imageView = new ImageView(new Image(imageUrl));
        setScene(new Scene(new Group(imageView)));
    }
}
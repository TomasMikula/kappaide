package kappa.ide.ui.editor;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import kappa.ide.filetype.FileType;
import kappa.ide.parsing.Highlighting;
import kappa.ide.parsing.StyleSheets;

import org.fxmisc.backcheck.InputChange.InputModification;
import org.fxmisc.easybind.EasyBind;
import org.fxmisc.easybind.monadic.MonadicBinding;
import org.fxmisc.livedirs.IOFacility;
import org.reactfx.EventSource;
import org.reactfx.EventStream;

/**
 * Tab pane with code editors.
 */
public class EditorPane {
    private static final Image SAVE_ICON = new Image(EditorPane.class.getResource("save-20.png").toString());
    private static final Image SAVE_ALL_ICON = new Image(EditorPane.class.getResource("save-all-20.png").toString());

    @SafeVarargs
    private static <T> T[] array(T... elements) {
        return elements;
    }

    private final TabPane tabPane = new TabPane();

    private final MonadicBinding<EditorTab> activeTab =
            EasyBind.monadic(tabPane.getSelectionModel().selectedItemProperty())
                    .map(tab -> (EditorTab) tab);

    private final ObservableValue<Boolean> activeTabSaved =
            activeTab.flatMap(EditorTab::savedProperty).orElse(true);

    private final ObservableValue<Boolean> allTabsSaved = EasyBind.<Boolean, Boolean>combine(
            EasyBind.map(tabPane.getTabs(), tab -> ((EditorTab) tab).savedProperty()),
            stream -> stream.allMatch(saved -> saved));

    private final Button saveButton = new Button(null, new ImageView(SAVE_ICON));
    private final Button saveAllButton = new Button(null, new ImageView(SAVE_ALL_ICON));
    private final HBox toolbar = new HBox(saveButton, saveAllButton);

    private final Parent node = new VBox(toolbar, tabPane);

    {
        saveButton.setOnAction(evt -> saveActive());
        saveButton.setTooltip(new Tooltip("Save"));
        saveButton.disableProperty().bind(activeTabSaved);

        saveAllButton.setOnAction(evt -> saveAll());
        saveAllButton.setTooltip(new Tooltip("Save all"));
        saveAllButton.disableProperty().bind(allTabsSaved);

        for(Button btn: array(saveButton, saveAllButton)) {
            btn.getStyleClass().add("toolbar-button");
        }

        // let tabPane fill the rest of the vertical space
        VBox.setVgrow(tabPane, Priority.ALWAYS);
    }

    // files currently being loaded
    private final Map<Path, CompletionStage<Void>> loadingFiles = new HashMap<>();

    // currently open files
    private final ObservableList<Path> openFiles = EasyBind.map(
            tabPane.getTabs(),
            t -> ((EditorTab) t).getPath());

    // stream to publish text changes to
    private final EventSource<InputModification<Path, String>> changeStream = new EventSource<>();

    // stream of tabs being closed
    private final EventSource<EditorTab> closingTabs = new EventSource<>();

    private final IOFacility ioFacility;

    public EditorPane(IOFacility ioFacility) {
        this.ioFacility = ioFacility;
    }

    public Node getNode() {
        return node;
    }

    public MonadicBinding<EditorTab> activeTabProperty() {
        return activeTab;
    }

    public EditorTab getActiveTab() {
        return activeTab.get();
    }

    public EventStream<InputModification<Path, String>> changeStream() {
        return changeStream;
    }

    public EventStream<EditorTab> closingTabs() {
        return closingTabs;
    }

    public ObservableList<Path> openFiles() {
        return openFiles;
    }

    public CompletionStage<Void> open(Path f) {
        assert f.isAbsolute();
        Optional<EditorTab> t = findOpenTab(f);
        if(t.isPresent()) {
            tabPane.getSelectionModel().select(t.get());
            return CompletableFuture.completedFuture(null);
        } else {
            return loadFile(f);
        }
    }

    public void forceClose(Path f) {
        findOpenTab(f).ifPresent(tab -> tab.forceClose());
    }

    public void applyHighlighting(Path f, Highlighting highlighting) {
        findOpenTab(f).ifPresent(tab -> tab.applyHighlighting(highlighting));
    }

    public boolean hasUnsavedChangesInActiveTab() {
        return !activeTabSaved.getValue();
    }

    public boolean hasUnsavedChangesInAnyTab() {
        return tabStream().anyMatch(EditorTab::isChanged);
    }

    public CompletionStage<Void> saveActive() {
        return activeTab.map(tab -> tab.save())
                .getOrElse(CompletableFuture.completedFuture(null));
    }

    /**
     * @throws NoSuchElementException if there is no open tab with the given
     * file in this EditorPane.
     * @return
     */
    public CompletionStage<Void> save(Path which) {
        return findOpenTab(which).get().save();
    }

    public CompletionStage<Void> saveAll() {
        return tabStream()
                .filter(EditorTab::isChanged)
                .map(tab -> tab.save())
                .reduce((a, b) -> a.thenCompose(x -> b))
                .orElse(CompletableFuture.completedFuture(null));
    }

    /**
     * Loads the content of the given file if its type is known,
     * otherwise asks the user what to do.
     */
    private CompletionStage<Void> loadFile(Path f) {
        FileType ft = FileType.of(f);
        if(ft == FileType.UNRECOGNIZED) {
            return askToOpenAsText(f);
        } else {
            return loadFile(f, ft);
        }
    }

    private CompletionStage<Void> askToOpenAsText(Path f) {
        ButtonType always = new ButtonType("Always");
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.initOwner(tabPane.getScene().getWindow());
        alert.setTitle("Unrecognized file type");
        alert.setContentText("Unrecognized file type. Do you want to open `"
                + f.getFileName() + "` as a text file?");
        alert.getButtonTypes().setAll(ButtonType.YES, always, ButtonType.CANCEL);

        ButtonType userResponse = alert.showAndWait().get();
        if(userResponse != ButtonType.CANCEL) {
            FileType ft;
            if(userResponse == always) {
                ft = FileType.define(FileType.TEXT.getIcon(), FileType.ext(f));
            } else {
                ft = FileType.TEXT;
            }
            return loadFile(f, ft);
        } else {
            return CompletableFuture.completedFuture(null);
        }
    }

    /**
     * Loads file content in a background thread.
     * When done loading, new EditorTab is added to this TabPane.
     */
    private CompletionStage<Void> loadFile(Path f, FileType ft) {
        CompletionStage<Void> loading = loadingFiles.get(f);
        if(loading != null) {
            return loading;
        } else {
            CompletionStage<Void> loaded = ioFacility.loadUTF8File(f).thenAcceptAsync(content -> {
                loadingFiles.remove(f);
                EditorTab t = new EditorTab(f, content, ioFacility);
                t.setGraphic(new ImageView(ft.getIcon()));
                StyleSheets.get(ft).ifPresent(sheet -> ((Parent) t.getContent()).getStylesheets().add(sheet));
                t.textValues().subscribe(text -> {
                    changeStream.push(new InputModification<>(f, text));
                });
                t.setOnClosed(evt -> closingTabs.push(t));
                tabPane.getTabs().add(t);
                tabPane.getSelectionModel().select(t);
            }, Platform::runLater);

            loadingFiles.put(f, loaded);

            return loaded;
        }
    }

    private Optional<EditorTab> findOpenTab(Path f) {
        for(Tab t: tabPane.getTabs()) {
            EditorTab tab = (EditorTab) t;
            if(tab.getPath().equals(f)) {
                return Optional.of(tab);
            }
        }
        return Optional.empty();
    }

    private Stream<EditorTab> tabStream() {
        return tabPane.getTabs().stream().map(t -> (EditorTab) t);
    }
}
package kappa.ide.ui.editor;

import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javafx.application.Platform;
import javafx.beans.value.ObservableBooleanValue;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import kappa.ide.parsing.Highlighting;

import org.fxmisc.livedirs.IOFacility;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.PlainTextChange;
import org.fxmisc.undo.UndoManager.UndoPosition;
import org.reactfx.EventStream;
import org.reactfx.EventStreams;
import org.reactfx.SuspendableNo;

import com.sun.javafx.scene.control.behavior.TabPaneBehavior;
import com.sun.javafx.scene.control.skin.TabPaneSkin;

public class EditorTab extends Tab {
    private final SuspendableNo forceClose = new SuspendableNo();

    private final CodeArea codeArea;
    private final Path path;
    private final IOFacility ioFacility;
    private final ObservableBooleanValue saved;

    public EditorTab(Path path, String content, IOFacility ioFacility) {
        this.path = path;
        this.codeArea = new CodeArea(content);
        this.ioFacility = ioFacility;
        codeArea.setParagraphGraphicFactory(LineNumberFactory.get(codeArea));
        saved = codeArea.getUndoManager().atMarkedPositionProperty();

        setTooltip(new Tooltip(path.toString()));
        setContent(codeArea);

        // indicate unsaved changes by an asterisk in tab title
        String savedTitle = path.getFileName().toString();
        String unsavedTitle = "*" + savedTitle;
        setText(codeArea.getUndoManager().isAtMarkedPosition() ? savedTitle : unsavedTitle);
        EventStreams.valuesOf(codeArea.getUndoManager().atMarkedPositionProperty())
                .map(saved -> saved ? savedTitle : unsavedTitle)
                .subscribe(title -> setText(title));

        setOnCloseRequest(evt -> handleCloseRequest(evt));
    }

    public Path getPath() {
        return path;
    }

    public EventStream<PlainTextChange> textChanges() {
        return codeArea.plainTextChanges();
    }

    public EventStream<String> textValues() {
        return EventStreams.valuesOf(codeArea.textProperty());
    }

    public ObservableBooleanValue beingUpdatedProperty() {
        return codeArea.beingUpdatedProperty();
    }

    public boolean isBeingUpdated() {
        return codeArea.isBeingUpdated();
    }

    public ObservableValue<Integer> caretPositionProperty() {
        return codeArea.caretPositionProperty();
    }

    public int getCaretPosition() {
        return codeArea.getCaretPosition();
    }

    public void requestClose() {
        TabPaneBehavior behavior = getBehavior();
        if(behavior.canCloseTab(this)) {
            behavior.closeTab(this);
        }
    }

    public void forceClose() {
        forceClose.suspendWhile(this::requestClose);
    }

    public CompletionStage<Void> save() {
        if(isSaved()) {
            return CompletableFuture.completedFuture(null);
        } else {
            String content = codeArea.getText();
            UndoPosition savedPos = codeArea.getUndoManager().getCurrentPosition();
            return ioFacility.saveUTF8File(path, content).thenRunAsync(savedPos::mark, Platform::runLater);
        }
    }

    public CompletionStage<Void> saveAndClose() {
        return save().thenRunAsync(() -> {
            if(isSaved()) { // only close if no further changes have been made
                requestClose();
            }
        }, Platform::runLater);
    }

    public ObservableBooleanValue savedProperty() {
        return saved;
    }

    public boolean isSaved() {
        return saved.get();
    }

    public boolean isChanged() {
        return !isSaved();
    }

    public void applyHighlighting(Highlighting highlighting) {
        codeArea.setStyleSpans(highlighting.offset(), highlighting.spans());
    }

    private void handleCloseRequest(Event evt) {
        if(isChanged() && !forceClose.getValue()) {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.initOwner(getTabPane().getScene().getWindow());
            alert.setTitle("Save changes before closing?");
            alert.setHeaderText("'" + path + "'\nhas unsaved changes.");
            alert.setContentText("Save before closing?");
            alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);

            ButtonType response = alert.showAndWait().get();
            if(response == ButtonType.YES) {
                evt.consume();
                saveAndClose().whenCompleteAsync((none, error) -> {
                    if(error != null) {
                        // TODO: log
                    }
                },
                Platform::runLater);
            } else if(response == ButtonType.CANCEL) {
                evt.consume();
            }
        }
    }

    private TabPaneBehavior getBehavior() {
        return ((TabPaneSkin) getTabPane().getSkin()).getBehavior();
    }
}
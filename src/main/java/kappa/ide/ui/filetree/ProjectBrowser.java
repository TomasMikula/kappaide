package kappa.ide.ui.filetree;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import kappa.ide.filetype.FileType;

import org.fxmisc.easybind.EasyBind;
import org.fxmisc.easybind.monadic.MonadicObservableValue;
import org.fxmisc.livedirs.DirectoryModel;
import org.fxmisc.livedirs.DirectoryModel.GraphicFactory;
import org.fxmisc.livedirs.IOFacility;
import org.reactfx.EventStream;
import org.reactfx.EventStreams;

public class ProjectBrowser {

    public static final GraphicFactory ICON_FACTORY = (path, isDir) -> isDir
            ? DirectoryModel.DEFAULT_GRAPHIC_FACTORY.createGraphic(path, true)
            : new ImageView(FileType.of(path).getIcon());

    private static final Image NEW_KAPPA_ICON = new Image(
            ProjectBrowser.class.getResource("new-kappa-file-20.png").toString());

    private static final Image NEW_DIR_ICON = new Image(
            ProjectBrowser.class.getResource("new-directory-20.png").toString());

    private static final Image DELETE_ICON = new Image(
            ProjectBrowser.class.getResource("delete-20.png").toString());

    @SafeVarargs
    private static <T> T[] array(T... elements) {
        return elements;
    }

    private final TreeView<Path> treeView = new TreeView<>();
    private final Button newKappaFileButton = new Button(null, new ImageView(NEW_KAPPA_ICON));
    private final Button newDirectoryButton = new Button(null, new ImageView(NEW_DIR_ICON));
    private final Button deleteButton = new Button(null, new ImageView(DELETE_ICON));

    private final MonadicObservableValue<TreeItem<Path>> selectedItem =
            EasyBind.monadic(treeView.getSelectionModel().selectedItemProperty());

    private final MonadicObservableValue<TreeItem<Path>> selectedItemDir =
            selectedItem.map(item -> item.isLeaf() ? item.getParent() : item);

    {
        treeView.setShowRoot(false);
        treeView.setCellFactory(tv -> new PathTreeCell());
        VBox.setVgrow(treeView, Priority.ALWAYS); // fill the extra vertical space

        BooleanBinding nothingSelected = Bindings.isNull(selectedItemDir);

        newKappaFileButton.disableProperty().bind(nothingSelected);
        newKappaFileButton.setOnAction(evt -> showNewKappaFileDialog(selectedItemDir.getOrThrow().getValue()));
        newKappaFileButton.setTooltip(new Tooltip("New Kappa File"));

        newDirectoryButton.disableProperty().bind(nothingSelected);
        newDirectoryButton.setOnAction(evt -> showNewDirectoryDialog(selectedItemDir.getOrThrow().getValue()));
        newDirectoryButton.setTooltip(new Tooltip("New Directory"));

        deleteButton.disableProperty().bind(Bindings.isNull(selectedItem));
        deleteButton.setOnAction(evt -> showConfirmDeleteDialog(treeView.getSelectionModel().getSelectedItems()));
        deleteButton.setTooltip(new Tooltip("Delete"));

        for(Button btn: array(newKappaFileButton, newDirectoryButton, deleteButton)) {
            btn.getStyleClass().add("toolbar-button");
        }
    }

    private final HBox toolbar = new HBox(newKappaFileButton, newDirectoryButton, deleteButton);
    private final Parent node = new VBox(toolbar, treeView);

    private final EventStream<Path> doubleClickedFiles =
            EventStreams.eventsOf(treeView, MouseEvent.MOUSE_CLICKED)
                    .filter(evt -> evt.getClickCount() == 2 && evt.getButton() == MouseButton.PRIMARY)
                    .filterMap(evt -> selectedItem.isPresent(), evt -> selectedItem.getOrThrow())
                    .filterMap(TreeItem::isLeaf, TreeItem::getValue);

    private final IOFacility ioFacility;


    public ProjectBrowser(TreeItem<Path> root, IOFacility ioFacility) {
        treeView.setRoot(root);
        this.ioFacility = ioFacility;
    }

    public Node getNode() {
        return node;
    }

    public EventStream<Path> doubleClickedFiles() {
        return doubleClickedFiles;
    }

    private void showNewKappaFileDialog(Path parentDir) {
        TextInputDialog dialog = new TextInputDialog(".ka");
        dialog.initOwner(node.getScene().getWindow());
        dialog.setTitle("New Kappa File");
        dialog.setHeaderText("Create new Kappa file in\n" + parentDir);
        dialog.setContentText("Enter file name:");

        Optional<String> fileName = dialog.showAndWait();
        fileName.filter(fn -> !fn.equals("")).ifPresent(fn -> {
            if(!(fn.endsWith(".ka") || fn.endsWith(".kappa"))) {
                fn += ".ka";
            }
            createNewFile(parentDir.resolve(fn));
        });
    }

    private void showNewDirectoryDialog(Path parentDir) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.initOwner(node.getScene().getWindow());
        dialog.setTitle("New Directory");
        dialog.setHeaderText("Create new sub-directory in\n" + parentDir);
        dialog.setContentText("Enter directory name:");
        Optional<String> dirName = dialog.showAndWait();

        dirName.filter(dn -> !dn.equals("")).ifPresent(dn -> {
            createNewDir(parentDir.resolve(dn));
        });
    }

    private void showConfirmDeleteDialog(List<TreeItem<Path>> items) {
        String what = items.size() == 1
                ? "'" + items.get(0).getValue().toString() + "'"
                : items.size() + " selected items";
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.initOwner(node.getScene().getWindow());
        alert.setHeaderText("Delete " + what + "?");
        alert.setTitle("Deletion Confirmation");

        Optional<ButtonType> response = alert.showAndWait();
        if(response.get() == ButtonType.OK) {
            delete(items);
        }
    }

    private void createNewFile(Path file) {
        ioFacility.createFile(file).whenCompleteAsync((none, error) -> {
            if(error != null) {
                // TODO: log
            }
        },
        Platform::runLater);
    }

    private void createNewDir(Path dir) {
        ioFacility.createDirectory(dir).whenCompleteAsync((none, error) -> {
            if(error != null) {
                // TODO: log
            }
        },
        Platform::runLater);
    }

    private void delete(List<TreeItem<Path>> items) {
        for(TreeItem<Path> item: items) {
            Path path = item.getValue();
            ioFacility.deleteTree(path).whenCompleteAsync((none, error) -> {
                if(error != null) {
                    // TODO: log
                }
            },
            Platform::runLater);
        }
    }
}

class PathTreeCell extends TreeCell<Path> {

    // make the cells display just the filename, instead of the whole path
    @Override
    public void updateItem(Path p, boolean empty) {
        super.updateItem(p, empty);
        if(!empty) {
            setGraphic(getTreeItem().getGraphic());
            setText(p.getFileName().toString());
        } else {
            setGraphic(null);
            setText(null);
        }
    }
}
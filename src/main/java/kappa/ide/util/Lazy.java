package kappa.ide.util;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class Lazy<T> implements Supplier<T>, AutoCloseable {

    private static final Consumer<?> NOOP = x -> {};

    @SuppressWarnings("unchecked")
    public static <T> Lazy<T> lazily(Supplier<T> provider) {
        return lazily(provider, (Consumer<T>) NOOP);
    }

    public static <T> Lazy<T> lazily(Supplier<T> provider, Consumer<T> disposer) {
        return new Lazy<>(provider, disposer);
    }

    private final Supplier<T> provider;
    private final Consumer<T> disposer;

    private boolean evaluated = false;
    private T value = null;

    private Lazy(Supplier<T> provider, Consumer<T> disposer) {
        this.provider = provider;
        this.disposer = disposer;
    }

    public boolean isEvaluated() {
        return evaluated;
    }

    @Override
    public T get() {
        if(!evaluated) {
            value = provider.get();
            evaluated = true;
        }
        return value;
    }

    @Override
    public void close() {
        if(evaluated) {
            disposer.accept(value);
        }
    }
}
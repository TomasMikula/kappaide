package kappa.ide.util;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.layout.Region;
import javafx.scene.transform.Scale;

public class ScalableContentPane extends Region {

    // content property
    private final ObjectProperty<Node> content = new SimpleObjectProperty<>();
    public ObjectProperty<Node> contentProperty() { return content; }
    public Node getContent() { return content.get(); }
    public void setContent(Node content) { this.content.set(content); }

    // aspect ratio proprety
    private final BooleanProperty keepAspectRatio = new SimpleBooleanProperty(true);
    public BooleanProperty keepAspectRatioProperty() { return keepAspectRatio; }
    public boolean isKeepAspectRatio() { return keepAspectRatio.get(); }
    public void setKeepAspectRatio(boolean keepAspectRatio) { this.keepAspectRatio.set(keepAspectRatio); }

    private final Scale contentScale = new Scale(1, 1, 0, 0);

    public ScalableContentPane() {
        this.content.addListener((obs, oldContent, newContent) -> {
            if(oldContent != null) {
                getChildren().clear();
                oldContent.getTransforms().remove(contentScale);
            }
            if(newContent != null) {
                newContent.getTransforms().add(contentScale);
                getChildren().setAll(newContent);
            }
        });
    }

    public ScalableContentPane(Node content) {
        this();
        this.content.set(content);
    }

    @Override
    protected void layoutChildren() {
        Node content = this.content.get();

        if(content == null) {
            return;
        }

        double contentWidth, contentHeight;
        if(content.getContentBias() == Orientation.HORIZONTAL) {
            contentWidth = content.prefWidth(-1);
            contentHeight = content.prefHeight(contentWidth);
        } else if(content.getContentBias() == Orientation.VERTICAL) {
            contentHeight = content.prefHeight(-1);
            contentWidth = content.prefWidth(contentHeight);
        } else { // null
            contentWidth = content.prefWidth(-1);
            contentHeight = content.prefHeight(-1);
        }

        double scaleX = getWidth() / contentWidth;
        double scaleY = getHeight() / contentHeight;

        double x, y;
        if(isKeepAspectRatio()) {
            double scale = Math.min(scaleX, scaleY);
            contentScale.setX(scale);
            contentScale.setY(scale);
            x = (getWidth() - scale * contentWidth) / 2;
            y = (getHeight() - scale * contentHeight) / 2;
        } else {
            contentScale.setX(scaleX);
            contentScale.setY(scaleY);
            x = 0;
            y = 0;
        }

        content.resizeRelocate(x, y, contentWidth, contentHeight);
    }

    @Override
    public Orientation getContentBias() {
        return content.get() != null
                ? content.get().getContentBias()
                : null;
    }

    @Override
    protected double computeMinWidth(double height) {
        return 1;
    }

    @Override
    protected double computeMinHeight(double width) {
        return 1;
    }

    @Override
    protected double computePrefWidth(double height) {
        return content.get() != null
                ? content.get().prefWidth(height)
                : 1;
    }

    @Override
    protected double computePrefHeight(double width) {
        return content.get() != null
                ? content.get().prefHeight(width)
                : 1;
    }

    @Override
    protected double computeMaxWidth(double height) {
        return Double.MAX_VALUE;
    }

    @Override
    protected double computeMaxHeight(double width) {
        return Double.MAX_VALUE;
    }
}

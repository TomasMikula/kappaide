package kappa.ide.parsing

import java.util.Optional
import kappa.syntax._
import kappa.parser.Parsers._
import scala.language.implicitConversions

object AstUtil {
  private implicit def optionToOptional[A](o: Option[A]): Optional[A] = o match {
    case Some(a) => Optional.of(a)
    case None => Optional.empty()
  }

  def ruleAt(ast: ParseResult[KappaFile], pos: Int): Optional[Rule] = ast match {
    case Success(kappaFile, _) => ruleAt(kappaFile, pos)
    case NoSuccess(msg, next) => Optional.empty()
  }

  def ruleAt(ast: SyntacticElement, pos: Int): Option[Rule] = ast match {
    case rule: Rule =>
      Some(rule)
    case _ =>
      ast.children.dropWhile(_.sourceRange.end < pos)
          .headOption.filter(_.sourceRange.start <= pos)
          .flatMap(ruleAt(_, pos))
  }
}
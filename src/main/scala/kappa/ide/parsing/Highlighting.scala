package kappa.ide.parsing

import java.util.Collections
import java.util.Set
import kappa.syntax._
import org.fxmisc.richtext.StyleSpans
import org.fxmisc.richtext.StyleSpans.singleton
import java.util.function.UnaryOperator
import java.util.HashSet

object Highlighting {
  import kappa.parser.Parsers._

  private type Style = Set[String]
  private type Spans = StyleSpans[Style]

  val ERROR = "error"
  val COMMENT = "comment"
  val INSTRUCTION = "instruction"
  val AGENT = "agent"
  val TOKEN = "token"
  val SITE = "site"
  val KEYWORD = "keyword"
  val PERTCMD = "pert-cmd"

  private val emptyStyle: Style = Collections.emptySet()
  private val errorStyle: Style = Collections.singleton(ERROR);

  private def styleFor(elem: SyntacticElement): Style = elem match {
    case Comment() => Collections.singleton(COMMENT)
    case _: Instruction => Collections.singleton(INSTRUCTION)
    case AgentTypeName(_) => Collections.singleton(AGENT)
    case TokenName(_) => Collections.singleton(TOKEN)
    case SiteName(_) => Collections.singleton(SITE)
    case _: Keyword => Collections.singleton(KEYWORD)
    case _: PertCmd => Collections.singleton(PERTCMD)
    case _ => emptyStyle
  }

  private def empty(offset: Int) = Highlighting(offset, singleton(emptyStyle, 0))

  def of(r: ParseResult[KappaFile]): Highlighting = r match {
    case Success(kappaFile, _) => of(kappaFile)
    case NoSuccess(msg, next) =>
      val offset = if(next.atEnd) next.offset - 1 else next.offset
      Highlighting(offset, singleton(errorStyle, 1))
  }

  private def of(elem: SyntacticElement): Highlighting = {
    val elemStyle = styleFor(elem)

    val left = empty(elem.sourceRange.start)
    val right = empty(elem.sourceRange.end)

    val glued = elem.children.map({ of(_) }).foldLeft(left){_ glue _} glue right

    if(elemStyle.isEmpty()) {
      glued
    } else {
      glued addStyle elemStyle
    }
  }
}

case class Highlighting(offset: Int, spans: Highlighting.Spans) {
  import scala.language.implicitConversions
  import Highlighting.{Style, emptyStyle}

  implicit def fnToUnaryOp[A](f: Function1[_ >: A, _ <: A]): UnaryOperator[A] = {
    new UnaryOperator[A]() { override def apply(a: A): A = f(a) }
  }

  def glue(that: Highlighting): Highlighting = {
    val thisEnd = this.offset + this.spans.length
    assert(that.offset >= thisEnd)
    val newHighlighting = if(that.offset == thisEnd) {
      this.spans concat that.spans
    } else {
      val glueLen = that.offset - thisEnd
      this.spans.append(emptyStyle, glueLen).concat(that.spans)
    }
    Highlighting(this.offset, newHighlighting);
  }

  def addStyle(style: Style): Highlighting =
    Highlighting(offset, spans mapStyles { s: Style => union(s, style) })

  private def union[A](a: Set[A], b: Set[A]): Set[A] =
    if(a.isEmpty()) {
      b
    } else if(b.isEmpty()) {
      a
    } else {
      val res = new HashSet[A](a.size() + b.size())
      res.addAll(a)
      res.addAll(b)
      res
    }
}

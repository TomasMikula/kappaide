package kappa.ide.parsing

import kappa.parser.Parsers
import kappa.parser.Parsers.ParseResult
import kappa.syntax.KappaFile

import org.fxmisc.backcheck.StatelessAnalyzer

class KappaParser extends StatelessAnalyzer[String, ParseResult[KappaFile]] {
  override def analyze(input: String) = Parsers.parseAll(Parsers.kappaFile, input)
}
package kappa.ide.visualization

import Math.{sin, cos, sqrt}
import javafx.geometry.{Bounds, Pos, VPos}
import javafx.scene.{Group, Node}
import javafx.scene.layout.{HBox, Region, StackPane, VBox}
import javafx.scene.paint.{Color, Paint}
import javafx.scene.shape.{Circle, CubicCurve, Line, Polygon}
import javafx.scene.text.{Font, FontWeight, Text, TextAlignment}
import javafx.scene.transform.{Rotate, Translate}
import kappa.syntax._
import org.fxmisc.easybind.EasyBind
import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

object RuleVisualization {

  private val ArrowLength = 50.0
  private val AgentRadius = 20.0
  private val SiteRadius = AgentRadius / 3
  private val StateRadius = SiteRadius
  private val AgentStrokeWidth = 2.0
  private val AnyBondLength = 7.0
  private val AnyBondRadius = 3.0
  private val WildcardBondFontSize = 10.0
  private val HGap = 15.0
  private val AgentGap = 30.0

  private class RuleSide(agents: List[Node], bonds: List[((Int, Node, Double, Double), (Int, Node, Double, Double))]) extends HBox(AgentGap, agents:_*) {
    setAlignment(Pos.CENTER)

    val links = bonds map { case ((_, site1, _, _), (_, site2, _, _)) =>
      val link = new CubicCurve
      link.getStyleClass().add("bond-link")
      link.setManaged(false)
      getChildren.add(link)
      val highlight = link.hoverProperty().or(site1.hoverProperty()).or(site2.hoverProperty())
      EasyBind.includeWhen(link.getStyleClass(), "highlight", highlight)
      EasyBind.includeWhen(site1.getStyleClass(), "highlight", highlight)
      EasyBind.includeWhen(site2.getStyleClass(), "highlight", highlight)
      link
    }

    override protected def layoutChildren(): Unit = {
      super.layoutChildren()
      for {
        (curve, ((i, _, ix, iy), (j, _, jx, jy))) <- links zip bonds
      } {
        val pi = agents(i).localToParent(ix, iy)
        val pj = agents(j).localToParent(jx, jy)
        val ctrlLen = (pj.getX() - pi.getX()) / 6
        val k = (AgentRadius + ctrlLen) / AgentRadius
        val pci = agents(i).localToParent(k*ix, k*iy)
        val pcj = agents(j).localToParent(k*jx, k*jy)
        curve.setStartX(pi.getX())
        curve.setStartY(pi.getY())
        curve.setControlX1(pci.getX())
        curve.setControlY1(pci.getY())
        curve.setControlX2(pcj.getX())
        curve.setControlY2(pcj.getY())
        curve.setEndX(pj.getX())
        curve.setEndY(pj.getY())
      }
    }
  }

  private case class ClassifiedAgentPattern(
      agent: AgentPattern,
      leftBound: List[(SitePattern, String, Int)],
      rightBound: List[(SitePattern, String, Int)],
      failBound: List[(SitePattern, String)],
      unlinked: List[SitePattern])

  private case class AgentPatternWithSiteAngles(
      agent: AgentPattern,
      bound: List[(SitePattern, Double, String, Int)],
      failBound: List[(SitePattern, Double, String)],
      unlinked: List[(SitePattern, Double)])

  private case class RenderedAgentPattern(
      agent: AgentPattern,
      graphic: Group,
      bonds: List[(Node, String, (Double, Double))])

  def apply(rule: Rule): Node = {
    val name = rule.name map { ruleName(_) }
    val preserved = longestPrefix(rule.lhsAgents, rule.rhsAgents)
    val lhs = ruleSide(rule.lhsAgents, rule.lhsTokens, preserved, "removed-agent")
    val rhs = ruleSide(rule.rhsAgents, rule.rhsTokens, preserved, "added-agent")
    val arrow = rule match {
      case r: ForwardRule => forwardArrow(r.rate)
      case r: BidiRule => bidiArrow(r.rate)
    }
    concat(name.toSeq :+ lhs :+ arrow :+ rhs)
  }

  private def ruleName(n: RuleName): Node = text(n.name)

  private def ruleSide(
      kappaExpr: KappaExpr,
      tokens: List[QuantifiedToken],
      preserved: Int,
      nonPreservedClass: String): Node = {

    val renderedAgents = renderAgents(positionSites(classifySites(kappaExpr)))

    val graphics = renderedAgents map { _.graphic }
    graphics.drop(preserved).foreach(_.getStyleClass.add(nonPreservedClass))

    val bondsByName = (for {
      (a, i) <- renderedAgents.zipWithIndex
      (site, name, (bx, by)) <- a.bonds
    } yield (i, site, name, bx, by)) groupBy { _._3 }
    val bonds = for {
      points <- bondsByName.values
      List((i, gi, _, ix, iy), (j, gj, _, jx, jy)) = points
    } yield ((i, gi, ix, iy), (j, gj, jx, jy))

    new RuleSide(graphics, bonds.toList)
  }

  private def longestPrefix(lhs: KappaExpr, rhs: KappaExpr): Int =
    longestPrefix(lhs.agents, rhs.agents, 0)

  @tailrec
  private def longestPrefix(lhs: List[AgentPattern], rhs: List[AgentPattern], acc: Int): Int = (lhs, rhs) match {
    case (Nil, _) => acc
    case (_, Nil) => acc
    case (l::ls, r::rs) =>
      if(l.typeName.name == r.typeName.name) {
        longestPrefix(ls, rs, acc+1)
      } else {
        acc
      }
  }

  private def classifySites(kappaExpr: KappaExpr): List[ClassifiedAgentPattern] = {
    val bondsWithAgentIndex = for {
      (a, i) <- kappaExpr.agents.zipWithIndex
      bondName <- namedBondsOf(a)
    } yield (bondName, i)

    val bondsToAgentIndex = bondsWithAgentIndex.foldLeft(Map[String, List[Int]]()) {
      case (m, (b, i)) => m.updated(b, i::m.getOrElse(b, Nil))
    }

    kappaExpr.agents.zipWithIndex.map { case (a, i) =>
      val leftBound = ListBuffer[(SitePattern, String, Int)]()
      val rightBound = ListBuffer[(SitePattern, String, Int)]()
      val failBound = ListBuffer[(SitePattern, String)]()
      for((s, b) <- sitesWithNamedBond(a)) {
        bondsToAgentIndex(b) match {
          case j::Nil => failBound += ((s, b))
          case j1::j2::j3::_ => failBound += ((s, b))
          case j1::j2::Nil => if(j1 == j2) {
            failBound += ((s, b))
          } else {
            val j = if(j1 == i) j2 else j1
            if(j < i) {
              leftBound += ((s, b, j))
            } else {
              rightBound += ((s, b, j))
            }
          }
        }
      }
      val unlinked = a.sites filter { _.bond match {
        case Some(NamedBond(_, _)) => false
        case _ => true
      }}
      ClassifiedAgentPattern(a, leftBound.toList, rightBound.toList, failBound.toList, unlinked)
    }
  }

  private def namedBondsOf(a: AgentPattern): List[String] =
    sitesWithNamedBond(a) map { case (s, b) => b }

  private def sitesWithBond(a: AgentPattern): List[(SitePattern, Bond)] = for {
    s <- a.sites
    b <- s.bond.toList
  } yield (s, b)

  private def sitesWithNamedBond(a: AgentPattern): List[(SitePattern, String)] = for {
    (s, NamedBond(_, BondName(bondName))) <- sitesWithBond(a)
  } yield (s, bondName)

  private def positionSites(agents: List[ClassifiedAgentPattern]): List[AgentPatternWithSiteAngles] = {
    agents map { a =>
      // sort by proximity of the neighbor
      val leftBound = a.leftBound sortBy { case (s, b, i) => -i }
      val rightBound = a.rightBound sortBy { case (s, b, i) => i }

      val nl = leftBound.size
      val nr = rightBound.size
      val nu = a.unlinked.size
      val nf = a.failBound.size
      val n = a.agent.sites.size
      assert(n == nl + nr + nu + nf)

      val (lPoints, uPoints, fPoints, rPoints) = if(nl > n - nl + 1) {
        val lPoints = layoutLeftUsePoles(nl)
        val (rPoints, ufPoints) = layoutRightNoPoles(nr + nu + nf).splitAt(nr)
        val (uPoints, fPoints) = ufPoints.splitAt(nu)
        (lPoints, uPoints, fPoints, rPoints)
      } else if(nr > n - nr + 1) {
        val rPoints = layoutRightUsePoles(nr)
        val (lPoints, fuPoints) = layoutLeftNoPoles(nl + nf + nu).splitAt(nl)
        val (fPoints, uPoints) = fuPoints.splitAt(nf)
        (lPoints, uPoints, fPoints, rPoints)
      } else if(nr > 0 || nl == 0) {
        val ruflPoints = layoutFromRight(nr + nu + nf + nl, nr % 2 == 1)
        val (rPoints, uflPoints) = ruflPoints.splitAt(nr)
        val (uPoints, flPoints) = uflPoints.splitAt(nu)
        val (fPoints, lPointsRev) = flPoints.splitAt(nf)
        (lPointsRev.reverse, uPoints, fPoints, rPoints)
      } else {
        val lfurPoints = layoutFromLeft(nl + nf + nu + nr, nl % 2 == 1)
        val (lPoints, furPoints) = lfurPoints.splitAt(nl)
        val (fPoints, urPoints) = furPoints.splitAt(nf)
        val (uPoints, rPointsRev) = urPoints.splitAt(nu)
        (lPoints, uPoints, fPoints, rPointsRev.reverse)
      }

      val l = (leftBound zip lPoints) map { case ((s, b, i), r) => (s, r, b, i) }
      val r = (rightBound zip rPoints) map { case ((s, b, i), r) => (s, r, b, i) }
      val f = (a.failBound zip fPoints) map { case ((s, b), r) => (s, r, b) }
      val u = (a.unlinked zip uPoints) map { case (s, r) => (s, r) }
      AgentPatternWithSiteAngles(a.agent, l ++ r, f, u)
    }
  }

  private def layoutRightUsePoles(n: Int): List[Double] = {
    require(n >= 2)

    val d = Math.PI / (n-1) // distance between sites (in radians)

    (n % 2) match {
      case 0 =>
        val odd = (0 until n/2) map { i => (0.5 + i) * d }
        val even = odd map { -_ }
        merge(odd, even)
      case 1 =>
        val even = (0 until (n-1)/2) map { i => (1 + i) * d }
        val odd = even map { -_ }
        0.0 :: merge(even, odd)
    }
  }

  private def layoutLeftUsePoles(n: Int): List[Double] =
    layoutRightUsePoles(n) map { Math.PI - _ }

  private def layoutRightNoPoles(n: Int): List[Double] =
    layoutRightUsePoles(n+2).take(n)

  private def layoutLeftNoPoles(n: Int): List[Double] =
    layoutRightNoPoles(n) map { Math.PI - _ }

  private def layoutFromRight(n: Int, usePole: Boolean): Seq[Double] = {
    val d = 2*Math.PI / n // distance between sites (in radians)
    val shift = if(usePole) 0 else d/2

    for {
      i <- 1 to n
      sgn = (i % 2) * 2 - 1
    } yield sgn * i/2 * d + shift
  }

  private def layoutFromLeft(n: Int, usePole: Boolean): Seq[Double] =
    layoutFromRight(n, usePole) map { Math.PI - _ }

  private def merge[A, A1 <: A, A2 <: A](l1: Seq[A1], l2: Seq[A2]): List[A] = {
    require(l1.size == l2.size)
    if(l1.isEmpty) {
      Nil
    } else {
      l1.head::l2.head::merge(l1.tail, l2.tail)
    }
  }

  private def renderAgents(agents: List[AgentPatternWithSiteAngles]): List[RenderedAgentPattern] = {
    for((a, i) <- agents.zipWithIndex) yield {
      val ac = agentCircle(a.agent)

      val (boundGraphics, bondPoints) = (for {
        (s, r, b, i) <- a.bound
        (graphic, bx, by) = boundSite(s, r)
        (abx, aby) = (cos(r) * AgentRadius + bx, -sin(r) * AgentRadius + by)
      } yield ((graphic, r), (graphic, b, (abx, aby)))).unzip
      val failBoundGraphics = a.failBound map { case (s, r, b) => (failBoundSite(s, r, b), r) }
      val unlinkedGraphics = a.unlinked map { case (s, r) => (unlinkedSite(s, r), r) }

      val siteGraphics = boundGraphics ++ failBoundGraphics ++ unlinkedGraphics map { case (gfx, r) =>
        gfx.setTranslateX(cos(r) * AgentRadius)
        gfx.setTranslateY(-sin(r) * AgentRadius)
        gfx
      }

      val g = new Group(ac::siteGraphics:_*)
      RenderedAgentPattern(a.agent, g, bondPoints)
    }
  }

  private def agentCircle(a: AgentPattern): Node =
    circle(a.typeName.name, AgentRadius, null, AgentStrokeWidth, Color.BLACK)

  private def site(s: SitePattern, angle: Double): (Group, Double, Double) = {
    val site = siteCircle(s.name.name)

    val (cx, cy) = (cos(angle), -sin(angle))
    if(s.state.isDefined) {
      val state = stateCircle(s.state.get.name.name)
      val tx = cx * (SiteRadius + StateRadius)
      val ty = cy * (SiteRadius + StateRadius)
      state.setTranslateX(tx)
      state.setTranslateY(ty)
      site.getChildren.add(state)
      (site, tx + cx * StateRadius, ty + cy * StateRadius)
    } else {
      (site, cx * SiteRadius, cy * SiteRadius)
    }
  }

  private def boundSite(s: SitePattern, angle: Double) = site(s, angle)

  private def failBoundSite(s: SitePattern, angle: Double, bondName: String): Node = {
    val (graphic, bx, by) = site(s, angle)
    val fb = failBond(angle, bondName)
    fb.setTranslateX(bx)
    fb.setTranslateY(by)
    graphic.getChildren.add(fb)
    graphic
  }

  private def unlinkedSite(s: SitePattern, angle: Double): Node = {
    val (graphic, bx, by) = site(s, angle)

    s.bond match {
      case None => graphic
      case Some(bond) => bond match {
        case AnyBond(_, _) =>
          val ab = anyBond(angle)
          ab.setTranslateX(bx)
          ab.setTranslateY(by)
          graphic.getChildren.add(ab)
          graphic
        case WildcardBond(_) =>
          val wb = wildcardBond(angle)
          wb.setTranslateX(bx)
          wb.setTranslateY(by)
          graphic.getChildren.add(wb)
          graphic
        case PatternBond(_, _, _) => graphic // TODO: display the pattern
      }
    }
  }

  private def wildcardBond(angle: Double): Group = {
    val text = this.text("?")
    text.setFont(Font.font(null, FontWeight.BOLD, WildcardBondFontSize))
    val res = bottomCentered(new StackPane(text))
    res.getTransforms().add(new Rotate(-(angle*180/Math.PI - 90)))
    res
  }

  private def failBond(angle: Double, bondName: String): Group = {
    val text = this.text(bondName)
    text.setFont(Font.font(null, FontWeight.BOLD, WildcardBondFontSize))
    text.setFill(Color.RED)
    val res = centered(new StackPane(text))
    res.getTransforms().add(new Translate(
        cos(angle) * WildcardBondFontSize/2,
        -sin(angle) * WildcardBondFontSize/2))
    res
  }

  private def text(txt: String): Text = {
    val t = new Text(txt)
    t.getStyleClass().add("text")
    t
  }

  private def anyBond(angle: Double): Group = {
    val x = cos(angle) * AnyBondLength
    val y = -sin(angle) * AnyBondLength

    val link = new Line(0, 0, x, y)
    link.getStyleClass().add("any-bond-link")

    val circle = new Circle(x, y, AnyBondRadius)
    link.getStyleClass().add("any-bond-circle")

    new Group(link, circle)
  }

  private def siteCircle(name: String): Group =
    circle(name, SiteRadius, Color.WHITE, AgentStrokeWidth/2, Color.BLACK)

  private def stateCircle(name: String): Group =
    circle(name, StateRadius, Color.WHITE, AgentStrokeWidth/2, Color.BLACK)

  private def circle(name: String, radius: Double, fill: Paint, strokeWidth: Double, stroke: Paint): Group = {
    val c = new Circle(radius)
    c.setFill(fill)
    c.setStroke(stroke)
    c.setStrokeWidth(strokeWidth)
    c.getStyleClass().add("circle")

    val t = text(name)
    t.setFont(new Font(1.5 * radius))

    centered(new StackPane(c, t))
  }

  private def centered(node: Region): Group = {
    val g = new Group(node)
    node.translateXProperty().bind(node.widthProperty().multiply(-0.5))
    node.translateYProperty().bind(node.heightProperty().multiply(-0.5))
    g
  }

  private def bottomCentered(node: Region): Group = {
    val g = new Group(node)
    node.translateXProperty().bind(node.widthProperty().multiply(-0.5))
    node.translateYProperty().bind(node.heightProperty().multiply(-1))
    g
  }

  private def forwardArrow(rate: Option[ForwardRate]): Node = {
    val arrow = rightArrow
    rate map { r => rateToText(r.rate) } match {
      case None => arrow
      case Some(r) =>
        val vbox = new VBox(r, arrow)
        vbox.setAlignment(Pos.CENTER)
        vbox
    }
  }

  private def bidiArrow(rate: Option[BidiRate]): Node = {
    val rArrow = rightArrow
    val lArrow = rightArrow
    lArrow.setScaleX(-1)
    rate match {
      case None => new VBox(rArrow, lArrow)
      case Some(BidiRate(_, r1, r2)) =>
        val fwRate = rateToText(r1)
        val bwRate = rateToText(r2)
        val vbox = new VBox(fwRate, rArrow, lArrow, bwRate)
        vbox.setAlignment(Pos.CENTER)
        vbox
    }
  }

  private def rightArrow: Group = {
    val halfLen = ArrowLength / 2
    val line = new Line(-halfLen, 0, halfLen, 0)
    line.setStrokeWidth(3.0)
    val triangle = new Polygon(0, -2, 0, 2, 2, 0)
    triangle.setFill(Color.BLACK)
    triangle.scaleXProperty().bind(line.strokeWidthProperty())
    triangle.scaleYProperty().bind(line.strokeWidthProperty())
    triangle.setTranslateX(halfLen)
    new Group(line, triangle)
  }

  private def rateToText(rate: RateExpr): Text = {
    val primary = numExprToStr(rate.primary)
    val txt = rate.uniMolecular map { numExprToStr(_) } match {
      case Some(expr) => s"$primary ($expr)"
      case None => primary
    }
    text(txt)
  }

  private def numExprToStr(expr: NumExpr): String = numExprToStr(expr, 0, 0)._1

  private def numExprToStr(expr: NumExpr, leftMinPrio: Int, rightMinPrio: Int): (String, Int, Int) = {

    val (str: String, lp: Int, rp: Int) = expr match {
      case IntegerLiteral(i) => (i.toString, 5, 5)
      case DoubleLiteral(d) => (d.toString, 5, 5)
      case VariableRef(v) => (v, 5, 5)
      case Infinity() => ("[inf]", 5, 5)
      case Pi() => ("[pi]", 5, 5)
      case EventCount() => ("[E]", 5, 5)
      case ProdEventCount() => ("[E+]", 5, 5)
      case NullEventCount() => ("[E-]", 5, 5)
      case EventLimit() => ("[Emax]", 5, 5)
      case BioTime() => ("[T]", 5, 5)
      case CpuTime() => ("[Tsim]", 5, 5)
      case BioTimeLimit() => ("[Tmax]", 5, 5)

      case PlusExpr(a, b) => (numExprToStr(a, 0, 1), numExprToStr(b, 1, 0)) match {
        case ((as, al, ar), (bs, bl, br)) => (s"$as + $bs", al, br)
      }
      case MinusExpr(a, b) => (numExprToStr(a, 0, 1), numExprToStr(b, 2, 0)) match {
        case ((as, al, ar), (bs, bl, br)) => (s"$as - $bs", al, br)
      }
      case TimesExpr(a, b) => (numExprToStr(a, 0, 3), numExprToStr(b, 3, 0)) match {
        case ((as, al, ar), (bs, bl, br)) => (s"$as * $bs", al, br)
      }
      case DivExpr(a, b) => (numExprToStr(a, 0, 3), numExprToStr(b, 4, 0)) match {
        case ((as, al, ar), (bs, bl, br)) => (s"$as / $bs", al, br)
      }
      case ModExpr(a, b) => (numExprToStr(a, 0, 3), numExprToStr(b, 4, 0)) match {
        case ((as, al, ar), (bs, bl, br)) => (s"$as [mod] $bs", al, br)
      }
      case PowExpr(a, b) => (numExprToStr(a, 0, 5), numExprToStr(b, 5, 0)) match {
        case ((as, al, ar), (bs, bl, br)) => (s"$as^$bs", al, br)
      }
      case MinExpr(a, b) => (numExprToStr(a, 5, 5), numExprToStr(b, 5, 5)) match {
        case ((as, al, ar), (bs, bl, br)) => (s"[min] $as $bs", 5, br)
      }
      case MaxExpr(a, b) => (numExprToStr(a, 5, 5), numExprToStr(b, 5, 5)) match {
        case ((as, al, ar), (bs, bl, br)) => (s"[max] $as $bs", 5, br)
      }

      case UnaryMinusExpr(arg) => numExprToStr(arg, 2, 0) match { case (s, l, r) => (s"-$s", 5, r) }
      case LogExpr(arg) => numExprToStr(arg, 4, 0) match { case (s, l, r) => (s"[log] $s", 5, r) }
      case SinExpr(arg) => numExprToStr(arg, 4, 0) match { case (s, l, r) => (s"[sin] $s", 5, r) }
      case CosExpr(arg) => numExprToStr(arg, 4, 0) match { case (s, l, r) => (s"[cos] $s", 5, r) }
      case TanExpr(arg) => numExprToStr(arg, 4, 0) match { case (s, l, r) => (s"[tan] $s", 5, r) }
      case SqrtExpr(arg) => numExprToStr(arg, 4, 0) match { case (s, l, r) => (s"[sqrt] $s", 5, r) }
      case ExpExpr(arg) => numExprToStr(arg, 4, 0) match { case (s, l, r) => (s"[exp] $s", 5, r) }
      case FloorExpr(arg) => numExprToStr(arg, 4, 0) match { case (s, l, r) => (s"[int] $s", 5, r) }
    }

    if(lp >= leftMinPrio && rp >= rightMinPrio) {
      (str, lp, rp)
    } else {
      (s"($str)", 5, 5)
    }
  }

  private def concat(nodes: Seq[Node]): Node = {
    val hbox = new HBox(HGap, nodes: _*)
    hbox.setFillHeight(false)
    hbox.setAlignment(Pos.CENTER)
    hbox
  }
}